#!/usr/bin/python3

#import telepot
#import datetime
import myUtils
#from TaskMsgQueue import TaskMsgQueue
#from UserListHandler import UserListHandler


# ------------------------------------------------------------------------------
# Register Users, the admin shall aprove new users.
class AccessRequestHandler:

    def __init__(self, adminIdFile, bot, userAccessList, userNotificationList, logger):
        self.m_adminId = 0
        self.m_pendingReqList = []
        self.m_adminIdFile = adminIdFile
        self.m_bot = bot
        self.m_userAccessList = userAccessList
        self.m_userNotificationList = userNotificationList
        self.m_logger = logger

    def initialize(self):
        try:
            with open(self.m_adminIdFile, 'r') as idfile:
                self.m_adminId = myUtils.tryInt(idfile.read().rstrip())
                self.m_logger.logText('Admin Id: ' + str(self.m_adminId))
        except IOError:
            self.m_logger.logText('Admin not yet defined.')

    def requestPermission(self, newUserFirstName, newUserLastName, newUserName, newUserId):
        if 0 == self.m_adminId:
            self.m_logger.logText('admin not yet defined...')
            self.setNewAdmin(newUserId)
            self.m_userAccessList.addUser(newUserId)
            self.m_userAccessList.storeList()
            self.m_userNotificationList.addUser(newUserId)
            self.m_userNotificationList.storeList()
        else:
            self.m_logger.logText('admin already defined...')
            self.sendRequestToAdmin(newUserFirstName, newUserLastName, newUserName, newUserId)
            self.addRequestToList(newUserId)

    def setNewAdmin(self, newUserId):
        with open(self.m_adminIdFile, 'w') as f:
            f.write(str(newUserId) + '\n')
            self.m_adminId = newUserId
            self.m_logger.logText('New registered admin: ' + str(newUserId))
            self.m_bot.sendMessage(self.m_adminId, 'You are registered as admin')

    def sendRequestToAdmin(self, newUserFirstName, newUserLastName, newUserName, newUserId):
        reqText = 'User [' + newUserFirstName + ' ' + newUserLastName + ' ' + newUserName + '] (ID: ' + str(newUserId) + ') requests access.'
        print(reqText)
        print(self.m_bot)
        print('"' + str(self.m_adminId) + '"')
        self.m_bot.sendMessage(308764185, 'gugugs')
        self.m_bot.sendMessage(self.m_adminId, reqText)
        self.m_logger.logText(reqText)

    def addRequestToList(self, newUserId):
        self.m_pendingReqList.append(newUserId)

    def ackNewUser(self, newUserId):
        newUserIdInt = myUtils.tryInt(newUserId)
        if True == self.isFeedbackCorrect(newUserIdInt):
            self.m_pendingReqList.remove(newUserIdInt)
            self.m_userAccessList.addUser(newUserIdInt)
            self.m_userAccessList.storeList()
            self.m_userNotificationList.addUser(newUserIdInt)
            self.m_userNotificationList.storeList()
            ackText = 'Your request was approved. Please send any message to start the game.'
            self.m_bot.sendMessage(newUserIdInt, ackText)
            self.m_logger.logText(ackText + ' (' + newUserId + ')')

    def rejectNewUser(self, newUserId):
        newUserIdInt = myUtils.tryInt(newUserId)
        if True == self.isFeedbackCorrect(newUserIdInt):
            self.m_pendingReqList.remove(newUserIdInt)
            rejectText = 'Your request was rejected.'
            self.m_bot.sendMessage(newUserIdInt, rejectText)
            self.m_logger.logText(rejectText + ' (' + newUserId + ')')

    def isFeedbackCorrect(self, newUserId):
        requestFound = False
        for user in self.m_pendingReqList:
            if user == newUserId:
                requestFound = True
        if False == requestFound:
            respText = 'No request pending to req: ' + str(newUserId)
            self.m_logger.logText(respText)
            self.m_bot.sendMessage(self.m_adminId, respText)
        return requestFound

    def showPendingRequests(self):
        testPendingReq = 'Pending req:\n'
        self.m_logger.logText('Pending Requests >>>')
        for req in self.m_pendingReqList:
            self.m_logger.logText(str(req))
            testPendingReq = testPendingReq + str(req) + '\n'
        self.m_logger.logText('Pending Requests <<<')
        self.m_bot.sendMessage(self.m_adminId, testPendingReq)

    def isAdmin(self, userId):
        retValue = False
        self.m_logger.logText('isAdmin')
        if userId == self.m_adminId:
            retValue = True
        else:
            responseText = 'Command requires admin previdges'
            self.m_logger.logText(responseText)
            self.m_bot.sendMessage(self.m_adminId, responseText)
        return retValue
