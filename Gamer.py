#!/usr/bin/python3

import datetime
import myUtils
from enum import Enum


## Gamer
#
# A single gamer plays his own game
class Gamer():

    class GameState(Enum):
        Startup = 0
        Question_01 = 1
        Question_02 = 2
        Question_03 = 3
        Question_04 = 4
        Done = 5

    ## Constructor
    def __init__(self, userId, firstName, lastName, userName, debugLogger):
        self.m_logger = debugLogger
        self.m_userId = userId
        self.m_name = 'Nobody'
        if '' != firstName:
            self.m_name = firstName
        elif '' != userName:
            self.m_name = userName
        elif '' != lastName:
            self.m_name = lastName
        self.m_state = Gamer.GameState.Startup

    # ------------------------------------------------------------------------------
    def getUserId(self):
        self.m_logger.logText('My user id is: ' + str(self.m_userId))
        return self.m_userId

    # ------------------------------------------------------------------------------
    def getName(self):
        return self.m_name

    # ------------------------------------------------------------------------------
    def play(self, command):
        response = 'no'
        self.m_logger.logText('game command: ' + str(command) + ' state: ' + str(self.m_state))
        if (Gamer.GameState.Startup == self.m_state):
            self.m_state = Gamer.GameState.Question_01
            response = ('Hi ' + self.getName() + '\nWelcome to the game!' +
            '\n\n' + 'Wie heiss Deine Firma?')
        elif (Gamer.GameState.Question_01 == self.m_state):
            if ('Komax' == command):
                response = 'yes!!' + '\n\n' + 'Wieviel ist 2 + 2'
                self.m_state = Gamer.GameState.Question_02

        elif (Gamer.GameState.Question_02 == self.m_state):
            if ('4' == command):
                response = 'yes!!' + '\n\n' + 'Differential von sin(x) / dx?'
                self.m_state = Gamer.GameState.Question_03

        elif (Gamer.GameState.Question_03 == self.m_state):
            if ('cos(x)' == command):
                response = 'yes!!' + '\n\n' + 'Wie viel ist 5! ?'
                self.m_state = Gamer.GameState.Question_04

        elif (Gamer.GameState.Question_04 == self.m_state):
            if ('120' == command):
                response = 'yes!! You did it!'
                self.m_state = Gamer.GameState.Done
        elif (Gamer.GameState.Done == self.m_state):
            response = 'You already finished the game.'
        else:
            response = 'aborted'
        return response
