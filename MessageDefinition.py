#!/usr/bin/python3

from enum import Enum

class TelegramMsg():

    class Msg(Enum):
        BootMsg = 0
        Terminate = 1

    def __init__(self, msgType, msgData):
        self.m_type = msgType
        self.m_data = msgData

    def getType(self):
        return self.m_type

    def getData(self):
        return self.m_data
