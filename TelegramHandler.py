#!/usr/bin/python3

import telepot
import datetime
import myUtils
import MessageDefinition
from MessageDefinition import TelegramMsg
from TaskMsgQueue import TaskMsgQueue
from UserListHandler import UserListHandler
from AccessRequestHandler import AccessRequestHandler
from GameController import GameController

## Telegram Handler
#
# Ensures that there is enough time between changing direction.
class TelegramHandler(TaskMsgQueue):

    ## Constructor
    def __init__(self, threadId, threadName, msgQueueSize, debugLogger, gameController):
        super(TelegramHandler, self).__init__(threadId, threadName, msgQueueSize, debugLogger)
        self.m_logger = debugLogger
        self.m_readyToRun = False
        self.m_bot = []
        self.m_userAccessList = []
        self.m_userNotificationList = []
        self.m_accessRequestHandler = []
        self.m_gameController = gameController

    # ------------------------------------------------------------------------------
    #
    def setup(self, botIdFile, accessListFile, observerListFile, adminIdFile):
        telegramId = self.__readTelegramId(botIdFile)

        self.m_userAccessList = UserListHandler(self.m_logger)
        self.m_userAccessList.initialize(accessListFile)
        self.m_userAccessList.loadList()
        #self.m_userAccessList.printList()

        self.m_userNotificationList = UserListHandler(self.m_logger)
        self.m_userNotificationList.initialize(observerListFile)
        self.m_userNotificationList.loadList()
        #self.m_userNotificationList.printList()

        if '' == telegramId:
            self.m_logger.logText('Internal telegram id not found. Create a file "cnfg/botId.txt" containing the ID of the bot.')
        else:
            self.m_logger.logText('Setup bot.')
            self.m_bot = telepot.Bot(telegramId)
            self.m_accessRequestHandler = AccessRequestHandler(adminIdFile,
                                                               self.m_bot,
                                                               self.m_userAccessList,
                                                               self.m_userNotificationList,
                                                               self.m_logger)
            self.m_accessRequestHandler.initialize()
            self.m_bot.message_loop(self.__handleTelegramMsg)
            self.m_readyToRun = True


    # ------------------------------------------------------------------------------
    #
    def terminateMsg(self):
        self.putMsgToQueue(TelegramMsg(TelegramMsg.Msg.Terminate, 0))

    # ------------------------------------------------------------------------------
    #
    def __sendMsgToUser(self, userId, text):
        if self.m_bot:
            self.m_bot.sendMessage(userId, text)

    # ------------------------------------------------------------------------------
    # Reads the telegram Id of this bot from cnfg/botId.txt
    def __readTelegramId(self, fileName):
        try:
            with open(fileName, 'r') as idfile:
                myId=idfile.read().rstrip()
        except IOError:
            myId=''
            self.m_logger.logText('File "' + fileName + '" not found.')
        return myId

    # ------------------------------------------------------------------------------
    #
    def __handleTelegramMsg(self, msg):
        print('got telegram msg')

        userId = self.__getIntKey2(msg, 'chat', 'id', -1)
        command = self.__getStringKey1(msg, 'text', '-')
        firstName = self.__getStringKey2(msg, 'from', 'first_name', 'NoFirstName')
        lastName = self.__getStringKey2(msg, 'from', 'last_name', 'NoLastName')
        userName = self.__getStringKey2(msg, 'from', 'username', 'NoUserName')

        self.m_logger.logText('-------------------------------------------')
        #self.m_logger.logText(str(msg))
        self.m_logger.logMessageCommandReceived(firstName, lastName, userName, userId, command)

        # -----
        # The only accessible command if the user is not registered
        if 'Reg' == command:
            self.m_accessRequestHandler.requestPermission(firstName, lastName, userName, userId)

        # -----
        # User commands for configuration, help, ...
        elif 'C' == command:
            if True == self.m_userAccessList.isUserRegistered(userId):
                self.m_bot.sendMessage(userId, str(datetime.datetime.now()))

        elif 'H' == command:
            if True == self.m_userAccessList.isUserRegistered(userId):
                self.__usageInformation(userId)

        elif 'Hw' == command:
            if True == self.m_userAccessList.isUserRegistered(userId):
                hwVersion = self.__getRaspberryPi_HW_Version()
                self.m_bot.sendMessage(userId, hwVersion)
                self.m_logger.logMessageWithUserId(userId, hwVersion)

        # -----
        # Admin commands
        elif 'Y' == command[0]:
            if True == self.m_accessRequestHandler.isAdmin(userId):
                self.m_accessRequestHandler.ackNewUser(command[2:])

        elif 'N' == command[0]:
            if True == self.m_accessRequestHandler.isAdmin(userId):
                self.m_accessRequestHandler.rejectNewUser(command[2:])

        elif 'Pr' == command:
            if True == self.m_accessRequestHandler.isAdmin(userId):
                self.m_accessRequestHandler.showPendingRequests()

        else:
            if True == self.m_userAccessList.isUserRegistered(userId):
                response = self.m_gameController.request(userId, command, firstName, lastName, userName)
                self.m_bot.sendMessage(userId, response)
                self.m_logger.logText(response)



    # ------------------------------------------------------------------------------
    # Extract string from first level key
    def __getStringKey1(self, testDict, keyName, defaultString):
        strValue = defaultString
        if keyName in testDict:
            strValue =  testDict[keyName]
        return strValue


    # ------------------------------------------------------------------------------
    # Extract string from second level key
    def __getStringKey2(self, testDict, keyName, keySubName, defaultString):
        strValue = defaultString
        if keyName in testDict:
            if keySubName in testDict[keyName]:
                strValue =  testDict[keyName][keySubName]
        return strValue


    # ------------------------------------------------------------------------------
    # Extract int from first level key
    def __getIntKey1(self, testDict, keyName, defaultValue):
        intValue = defaultValue
        if keyName in testDict:
            intValue =  myUtils.tryInt(testDict[keyName], defaultValue)
        return intValue


    # ------------------------------------------------------------------------------
    # Extract int from second level key
    def __getIntKey2(self, testDict, keyName, keySubName, defaultValue):
        intValue = defaultValue
        if keyName in testDict:
            if keySubName in testDict[keyName]:
                intValue =  myUtils.tryInt(testDict[keyName][keySubName], defaultValue)
        return intValue


    # ------------------------------------------------------------------------------
    # Print software infos
    def __usageInformation(self, userId):
        #helpText = str('Garage Door Controller - ' + ProjectVersion.getVersionNumber() +
        helpText = str('Komax Game Bot' +
                   '\n\nSend the following messages to the bot:\n' +
                   '   C: Clock, to get the current time.\n' +
    #               '   Reg: to REGISTER yourself. You will get state updates.\n' +
                   '   H: print this HELP.\n' +
                   '   Hw: print the HW version of the Raspberry Pi.\n' +
                   '   Any command to start the game.\n' +
                       '\n(c) by reto271\n')
        self.m_logger.logMultiLineText(userId, helpText)
        if '' != self.m_bot:
            self.m_bot.sendMessage(userId, helpText)

    # ------------------------------------------------------------------------------
    # Get Raspberry Pi HW Info from the cpuinfo file
    def __getRaspberryPi_HW_Version(self):
        myHW_Info = "-"
        try:
            f = open('/proc/cpuinfo','r')
            for line in f:
                if line[0:5]=='Model':
                    length=len(line)
                    myHW_Info = line[9:length-1]
            f.close()
        except:
            myHW_Info = "unknown"
        return myHW_Info
